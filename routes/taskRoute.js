const express= require("express")
const router= express.Router();
const taskController= require("../controllers/taskController")

// [SECTION] Get all tasks
router.get("/", (request, response)=> {

	// invokes the getAllTasks function from the taskController.js
	taskController.getAllTasks().then(resultFromController=> response.send(resultFromController))
})

// [SECTION] Create new task
router.post("/", (request, response)=> {
	taskController.createTask(request.body).then(resultFromController => response.send(resultFromController))
})
// [SECTION] Delete a task
router.delete("/:id", (request, response)=> {
	taskController.deleteTask(request.params.id).then(resultFromController=> response.send(resultFromController))
})


// [SECTION] Update a task
router.put("/:id", (request, response)=>{
	taskController.updateTask(request.params.id, request.body).then(resultFromController=> response.send(resultFromController));
})



 
module.exports= router;

