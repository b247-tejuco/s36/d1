const Task= require("../models/Model")

module.exports.getAllTasks= () => {
	return Task.find({}).then((result)=> {
		return result;
	})
}

// The request body coming from the client was passed from the "taskRoute.js" file via request.body as an argument and is renamed a "requestBody" parameter in the controller file
module.exports.createTask= (requestDotBody)=> {
	// Creates a task object based on the Mongoose model "taskModel"
	let newTask= new Task({
		name: requestDotBody.name
	})

	return newTask.save().then((task, error)=> {
		if(error){
			console.error(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask= (taskId)=> {
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=> {
		if(err){
			console.err(err);
			return false;
		} else{
			return removedTask;
		}
	})
}


module.exports.updateTask= (taskId, newContent)=> {

	// The findById, is a mongoose method that will look for a task with the same id provided from the URL
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(error);
			return false;
		}

		// Result of the "findById" method will be stored in the result parameter
		result.name= newContent.name;

		// saves the updated object in the mongoDb database
		return result.save().then((updateTask, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updateTask;
			}
		})
	})
}